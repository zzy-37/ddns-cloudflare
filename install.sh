#/bin/sh

systemd_unit_path=/etc/systemd/system
service_file=$systemd_unit_path/ddns-update.service
timer_file=$systemd_unit_path/ddns-update.timer

[ "$1" = -u ] && {
    rm -v $service_file $timer_file
    exit
}

cd $(dirname $0)

>$service_file echo "\
[Unit]
Description=DDNS update script
Documentation=file://$PWD/README.md
After=network.target nss-lookup.target

[Service]
Type=oneshot
ExecStart=$(which python3) $PWD/cf-update.py" &&
    echo "wrote service file: $service_file"

>$timer_file echo "\
[Unit]
Description=DDNS update in every 10 minutes
Documentation=file://$PWD/README.md

[Timer]
OnCalendar=*-*-* *:00/10:00

[Install]
WantedBy=timers.target" &&
    echo "wrote timer file: $timer_file"
