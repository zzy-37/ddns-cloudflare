# DDNS Cloudflare

Simple python script to update cloudflare dns record, can be used as simple ddns update client

Cloudflare API v4 Documentation: https://api.cloudflare.com/

## Usage

First edit the file `cf-update.py` to set these global vairables: `email`, `api-key`, `zone_name`, `record_name`

```python
email       = 'example@example.com'                   # Email address associated with your account
api_key     = '1234567893feefc5f0q5000bfo0c38d90bbeb' # API key generated on the "My Account" page
zone_name   = 'example.com'                           # the base domain name of cloudflare zone
record_name = 'www.example.com'                       # the dns record you want to update
```

Then just run:

```console
$ python3 cf-update.py
```

this will update the dns record with your machine's public ip address

## Use as ddns client

### systemd.timer

The `install.sh` script installs two systemd units `ddns-update.service` and `ddns-update.timer` to `/etc/systemd/system/`, which is set to run the updating script for every 10 minutes. For more info, see [systemd/Timers](https://wiki.archlinux.org/title/Systemd/Timers).

Run the following commands with root privilege:

```console
# ./install.sh                          # install the unit files
# systemctl start  ddns-update.timer    # starts the timer
# systemctl enable ddns-update.timer    # enable the timer on start up
```

now every 10 minutes the script will check your ip and update it if it has changed.

you can examine the command output with:

```console
$ journalctl -u ddns-update.service
```

to uninstall, simply run:

```console
# ./install.sh -u                       # remove the unit files
```

### cron job

[cron](https://en.wikipedia.org/wiki/Cron)

One liner:

```console
$ { crontab -l; echo "*/10 * * * * $(which python3) $(realpath cf-update.py) >/var/tmp/cf-ddns.log"; } | crontab -
```
