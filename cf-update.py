#/usr/bin/env python3
# script to update cloudflare dns record
# set the following global variables to query the api, see: https://api.cloudflare.com/#getting-started-requests

email       = 'example@example.com'                   # Email address associated with your account
api_key     = '1234567893feefc5f0q5000bfo0c38d90bbeb' # API key generated on the "My Account" page
zone_name   = 'example.com'                           # the base domain name of cloudflare zone
record_name = 'www.example.com'                       # the dns record you want to update

endpoints   = 'https://api.cloudflare.com/client/v4/' # cloudflare api endpoints
cache_file  = '/var/tmp/cf-ddns.cache.json'           # cached dns record information

import urllib.request
import json
from os.path import exists
from datetime import datetime

def cf_api(query, method='GET', data=None): # -> response: dict
    headers = {
            'X-Auth-Email': email,
            'X-Auth-Key': api_key,
            'Conent-Type': 'applicaiton/json'
            }
    req = urllib.request.Request(url= endpoints + query, data=data, headers=headers, method=method)
    print(method, query)
    with urllib.request.urlopen(req) as res:
        body = json.loads(res.read().decode('utf-8'))
        if not body['success']:
            print('error:', body['errors'])
        return body['result']

def get_myip():
    # with urllib.request.urlopen('https://checkip.amazonaws.com') as res:
    #     return res.read().decode('utf-8').strip()
    with urllib.request.urlopen('https://httpbin.org/get') as res:
        return json.loads(res.read().decode('utf-8'))['origin']

def get_record():
    if exists(cache_file):
        print('found cache file:', cache_file)
        with open(cache_file) as f:
            cache = json.load(f)
            if cache['name'] == record_name:
                print('using cached file')
                return cache
            else:
                print('invalid cache')
    else:
        print('no cache file')
    zone_id = cf_api(f'zones?name={zone_name}')[0]['id']
    record = cf_api(f'zones/{zone_id}/dns_records?name={record_name}')[0]
    dump_cache(record)
    return record

def update_record(record, ip):
    data = json.dumps({
        'type': 'A',
        'name': record['name'],
        'content': ip,
        'ttl': 1,
        'proxied': False
        }).encode('utf-8')
    result = cf_api(f'zones/{record["zone_id"]}/dns_records/{record["id"]}', 'PUT', data)
    dump_cache(result)

def dump_cache(record):
    with open(cache_file, 'w') as f:
        json.dump(record, f, indent=2)
    print('cached dns record to', cache_file)

if __name__ == '__main__':
    print('log start:', datetime.now())
    try:
        ip = get_myip()
        print('local machine has address', ip)
        record = get_record()
        if record['content'] == ip:
            print("local ip address didn't change, no need to update")
        else:
            update_record(record, ip)
    except Exception as e:
        print(f'{type(e).__name__}: {e}')
    print('---log end---\n')
